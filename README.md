# DeepGeoStat WP5 - Python code for solar panel detection using deep learning

Boonstra, Harm Jan; De Jong, Tim; Krieg, Sabine

Python code for detecting whether solar panels are present in aerial photos has been
developed under the DeepSolaris and DeepGeoStat projects. In this particular Python notebook
a ResNet50 convolutional neural network is trained on a dataset containing high-resolution
annotated aerial images of two regions in the south of the Netherlands. Many
photos in this dataset have been annotated multiple times, and the annotations are
not always unanimous. The dataset of aerial images and annotations can be downloaded
from https://zenodo.org/record/7233404#.Y1JS-NfP1D8.

The notebook demonstrates among other things how to apply data augmentation,
how to oversample the minority class during training, how to train using a binomial cross-entropy loss function suitable for
multiple annotations, and how to apply transfer learning. The code uses the PyTorch computational framework, supporting the
use of a GPU. The trained model is then applied to a test-set to measure the prediction performance.

This research was conducted under:

- ESS action 'Merging Geostatistics and Geospatial Information in Member States' (grant agreement no.: 08143.2017.001-2017.408),
- ESS topic B5674-2020-GEOS (project 101033951 2020-NL-GEOS-DEEP-GEO-STAT),
- a research program of Statistics Netherlands (https://www.cbs.nl)
